/*
 * Copyright (C) 2014 Sony Mobile Communications AB.
 * All rights, including trade secret rights, reserved.
 */
 
#include "../cfgdefault/CFG_Custom1_Default.h"
#include "../cfgdefault/CFG_GPS_Default.h"
#include "../cfgdefault/CFG_PRODUCT_INFO_Default.h"
#include "../cfgdefault/CFG_WIFI_Default.h"
//CEI comments start //
//LE:A Add NVRAM
#include "../cfgdefault/CFG_SW_VER_Default.h"
#include "../cfgdefault/CFG_PROJ_ID_Default.h"
//CEI comments end //
#include "../cfgdefault/CFG_P_SENSOR_THRESHOLD_Default.h"

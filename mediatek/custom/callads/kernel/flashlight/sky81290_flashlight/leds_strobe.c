#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/types.h>
#include <linux/wait.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/poll.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/cdev.h>
#include <linux/errno.h>
#include <linux/time.h>
#include "kd_flashlight.h"
#include <asm/io.h>
#include <asm/uaccess.h>
#include "kd_camera_hw.h"
#include <cust_gpio_usage.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/xlog.h>
#include <linux/version.h>

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,37))
#include <linux/mutex.h>
#else
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27)
#include <linux/semaphore.h>
#else
#include <asm/semaphore.h>
#endif
#endif

/******************************************************************************
 * Debug configuration
******************************************************************************/
// availible parameter
// ANDROID_LOG_ASSERT
// ANDROID_LOG_ERROR
// ANDROID_LOG_WARNING
// ANDROID_LOG_INFO
// ANDROID_LOG_DEBUG
// ANDROID_LOG_VERBOSE
#define TAG_NAME "leds_strobe.c"
#define PK_DBG_NONE(fmt, arg...)    do {} while (0)
#define PK_DBG_FUNC(fmt, arg...)    xlog_printk(ANDROID_LOG_DEBUG  , TAG_NAME, KERN_INFO  "%s: " fmt, __FUNCTION__ ,##arg)
#define PK_WARN(fmt, arg...)        xlog_printk(ANDROID_LOG_WARNING, TAG_NAME, KERN_WARNING  "%s: " fmt, __FUNCTION__ ,##arg)
#define PK_NOTICE(fmt, arg...)      xlog_printk(ANDROID_LOG_DEBUG  , TAG_NAME, KERN_NOTICE  "%s: " fmt, __FUNCTION__ ,##arg)
#define PK_INFO(fmt, arg...)        xlog_printk(ANDROID_LOG_INFO   , TAG_NAME, KERN_INFO  "%s: " fmt, __FUNCTION__ ,##arg)
#define PK_TRC_FUNC(f)              xlog_printk(ANDROID_LOG_DEBUG  , TAG_NAME,  "<%s>\n", __FUNCTION__);
#define PK_TRC_VERBOSE(fmt, arg...) xlog_printk(ANDROID_LOG_VERBOSE, TAG_NAME,  fmt, ##arg)
#define PK_ERROR(fmt, arg...)       xlog_printk(ANDROID_LOG_ERROR  , TAG_NAME, KERN_ERR "%s: " fmt, __FUNCTION__ ,##arg)


#define DEBUG_LEDS_STROBE
#ifdef  DEBUG_LEDS_STROBE
	#define PK_DBG PK_DBG_FUNC
	#define PK_VER PK_TRC_VERBOSE
	#define PK_ERR PK_ERROR
#else
	#define PK_DBG(a,...)
	#define PK_VER(a,...)
	#define PK_ERR(a,...)
#endif

/******************************************************************************
 * local variables
******************************************************************************/

static DEFINE_SPINLOCK(g_strobeSMPLock); /* cotta-- SMP proection */


static u32 strobe_Res = 0;
static u32 strobe_Timeus = 0;
static BOOL g_strobe_On = 0;
static int g_led_state = 0;
//CEI comments start
//[BugID:1269]2015/03/13 Jiahan Li:Sometimes torch keeps lighting on after ESTA test run.
static unsigned int g_duty=-1;
//CEI comments end
static int g_timeOutTimeMs=0;

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,37))
static DEFINE_MUTEX(g_strobeSem);
#else
static DECLARE_MUTEX(g_strobeSem);
#endif


#define STROBE_DEVICE_ID 0xC6


static struct work_struct workTimeOut;

//@@ #define FLASH_GPIO_ENF GPIO12
//@@ #define FLASH_GPIO_ENT GPIO13

/*****************************************************************************
Functions
*****************************************************************************/
static void work_timeOutFunc(struct work_struct *data);

static int FL_preOn(void)
{
    //CEI comments start
    //[BugID:1269]2015/03/13 Jiahan Li:Sometimes torch keeps lighting on after ESTA test run.
    PK_DBG("g_led_state=%d, g_duty=%u\n", g_led_state, g_duty);
    //CEI comments end
    //CEI comments start
    //[BugID:371]:2014/10/28 Jiahan Li: Use duty to distinguish flash or torch
    if (g_duty == 0) 
        g_led_state = 1;
    else
        g_led_state = 2;
    //CEI comments end
    //CEI comments start
    //[BugID:501]:2014/11/13 Jiahan Li: Function has no return
    return 0;
	//CEI comments end
}


int FL_Enable(void)
{
    //CEI comments start//
    //Move torch enable code here to match calling procedure.
    int i = 0;
    PK_DBG("FL_Enable g_led_state=%d\n", g_led_state);    
    if (g_led_state == 2) {
        //CEI comments start
        //[BugID:1269]2015/03/13 Jiahan Li:Move the flash current ration setting here because to disable torch would clear the setting
		for (i=0; i<20; i++) {
            mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ZERO);
            //udelay(2);
            mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ONE);
        }
        udelay(500);

        for (i=0; i<4; i++) {
            mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ZERO);
            //udelay(2);
            mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ONE);
        }
        udelay(500);
        //CEI comments end
        mt_set_gpio_out(GPIO_CAMERA_FLASH_EN_PIN, GPIO_OUT_ONE);        
    } else if (g_led_state == 1) {
        for (i=0; i<19; i++) {
            mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ZERO);
            mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ONE);
        }
        udelay(500);

        for (i=0; i<4; i++) {
            mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ZERO);
            mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ONE);
        }
        udelay(500);
    }
    PK_DBG("sky81290 FL_Enable line=%d\n",__LINE__);
    //CEI comments end//
    return 0;
}



int FL_Disable(void)
{
    int i = 0;

    if (g_led_state == 2) {
	     mt_set_gpio_out(GPIO_CAMERA_FLASH_EN_PIN, GPIO_OUT_ZERO);
    } else if (g_led_state == 1) {
        //CEI comments start
        //[BugID:1269]2015/03/13 Jiahan Li:Use a reset A2C pattern to disable torch
        /*
         //CEI comments start//
         //To fix the torch mode address
         for (i=0; i<19; i++) {
         //CEI comments end//
             mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ZERO);
             udelay(2);
             mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ONE);
         }
         udelay(500);

         for (i=0; i<1; i++) {
             mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ZERO);
             udelay(2);
             mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ONE);
         }
         udelay(500);
        */
        mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ONE);
        udelay(2);
        mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ZERO);
		udelay(500);
        //CEI comments end
    }
    g_led_state = 0;
    PK_DBG("sky81290 FL_Disable line=%d\n",__LINE__);

    return 0;
}

int FL_dim_duty(kal_uint32 duty)
{
    //CEI comments start
    //[BugID:1269]2015/03/13 Jiahan Li:Sometimes torch keeps lighting on after ESTA test run.
    PK_DBG("duty=%u\n", duty);
    //CEI comments end
    g_duty =  duty;
    return 0;
}


int FL_Init(void)
{
    int i=0;
    
    if(mt_set_gpio_mode(GPIO_CAMERA_FLASH_EN_PIN,GPIO_MODE_00)){PK_DBG("[sky81290_flashlight] set gpio mode failed!! \n");}
    if(mt_set_gpio_dir(GPIO_CAMERA_FLASH_EN_PIN,GPIO_DIR_OUT)){PK_DBG("[sky81290_flashlight] set gpio dir failed!! \n");}
    if(mt_set_gpio_out(GPIO_CAMERA_FLASH_EN_PIN,GPIO_OUT_ZERO)){PK_DBG("[sky81290_flashlight] set gpio failed!! \n");}
    /*Init. to disable*/
    if(mt_set_gpio_mode(GPIO_CAMERA_FLASH_MODE_PIN,GPIO_MODE_00)){PK_DBG("[sky81290_flashlight] set gpio mode failed!! \n");}
    if(mt_set_gpio_dir(GPIO_CAMERA_FLASH_MODE_PIN,GPIO_DIR_OUT)){PK_DBG("[sky81290_flashlight] set gpio dir failed!! \n");}
    if(mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN,GPIO_OUT_ZERO)){PK_DBG("[sky81290_flashlight] set gpio failed!! \n");}

    if(mt_set_gpio_mode(GPIO_CAMERA_FLASH_EXT1_PIN,GPIO_MODE_00)){PK_DBG("[sky81290_flashlight] set gpio mode failed!! \n");}
    if(mt_set_gpio_dir(GPIO_CAMERA_FLASH_EXT1_PIN,GPIO_DIR_OUT)){PK_DBG("[sky81290_flashlight] set gpio dir failed!! \n");}
    if(mt_set_gpio_out(GPIO_CAMERA_FLASH_EXT1_PIN,GPIO_OUT_ZERO)){PK_DBG("[sky81290_flashlight] set gpio failed!! \n");}

    INIT_WORK(&workTimeOut, work_timeOutFunc);
    PK_DBG("sky81290 FL_Init line=%d\n",__LINE__);
    return 0;
}


int FL_Uninit(void)
{
    //CEI comments start
    //[BugID:1269]2015/03/13 Jiahan Li:Use a reset pattern and disable flash to disable all. 
    //FL_Disable();
    mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ONE);
    udelay(2);
    mt_set_gpio_out(GPIO_CAMERA_FLASH_MODE_PIN, GPIO_OUT_ZERO);
    udelay(500);
	mt_set_gpio_out(GPIO_CAMERA_FLASH_EN_PIN, GPIO_OUT_ZERO);
	PK_DBG("line=%d\n", __LINE__);
    //CEI comments end
    return 0;
}
//CEI comments start
//[BugID:370]: 2014/10/28 Jiahan Li: Don't do any low power detection
int FL_hasLowPowerDetect()
{

	return 0;
}
//CEI comments end

/*****************************************************************************
User interface
*****************************************************************************/

static void work_timeOutFunc(struct work_struct *data)
{
    //CEI comments start
    //[BugID:1269]2015/03/13 Jiahan Li:Remove the unnecessary function call
    //FL_Disable();
    //CEI comments end
    PK_DBG("sky81290 ledTimeOut_callback\n");
    //printk(KERN_ALERT "work handler function./n");
}



enum hrtimer_restart ledTimeOutCallback(struct hrtimer *timer)
{
    schedule_work(&workTimeOut);
    return HRTIMER_NORESTART;
}
static struct hrtimer g_timeOutTimer;
void timerInit(void)
{
    g_timeOutTimeMs=1000; //1s
    hrtimer_init( &g_timeOutTimer, CLOCK_MONOTONIC, HRTIMER_MODE_REL );
    g_timeOutTimer.function=ledTimeOutCallback;

}



static int constant_flashlight_ioctl(MUINT32 cmd, MUINT32 arg)
{
    //CEI comments start//
    //For FLASH_IOC_GET_PRE_ON_TIME_MS
    int temp;
    //CEI comments end//
    int i4RetValue = 0;
    int ior_shift;
    int iow_shift;
    int iowr_shift;
    ior_shift = cmd - (_IOR(FLASHLIGHT_MAGIC,0, int));
    iow_shift = cmd - (_IOW(FLASHLIGHT_MAGIC,0, int));
    iowr_shift = cmd - (_IOWR(FLASHLIGHT_MAGIC,0, int));
    PK_DBG("sky81290_flashlight_ioctl() line=%d ior_shift=%d, iow_shift=%d iowr_shift=%d arg=%d\n",__LINE__, ior_shift, iow_shift, iowr_shift, arg);
    switch(cmd)
    {
	    case FLASH_IOC_SET_TIME_OUT_TIME_MS:
            PK_DBG("FLASH_IOC_SET_TIME_OUT_TIME_MS: %d\n",arg);
            g_timeOutTimeMs=arg;
            break;


    	case FLASH_IOC_SET_DUTY :
    	    PK_DBG("FLASHLIGHT_DUTY: %d\n",arg);
    	    FL_dim_duty(arg);
    	    break;


    	case FLASH_IOC_SET_STEP:
    	    PK_DBG("FLASH_IOC_SET_STEP: %d\n",arg);

    	    break;

        case FLASH_IOC_PRE_ON:
            PK_DBG("FLASH_IOC_PRE_ON: %d\n", arg);
            FL_preOn();
            break;
            
        //CEI comments start//
        //Add this case to let upper layer know it could send FLASH_IOC_PRE_ON command
        case FLASH_IOC_GET_PRE_ON_TIME_MS:
            PK_DBG("FLASH_IOC_GET_PRE_ON_TIME_MS: %d\n",arg);
            temp=13;
            if (copy_to_user((void __user *) arg , (void*)&temp , 4)) {
                PK_DBG("ioctl copy to user failed\n");
                return -1;
            }
            break;
        //CEI comments end//
            
    	case FLASH_IOC_SET_ONOFF :
            //CEI comments start
            //[BugID:1269]2015/03/13 Jiahan Li:Sometimes torch keeps lighting on after ESTA test run.
    	    PK_DBG("FLASH_IOC_SET_ONOFF: %d, g_led_state=%d\n",arg, g_led_state);
            //CEI comments end
    	    if (arg==1) {
                if (g_timeOutTimeMs!=0) {
                    ktime_t ktime;
                    ktime = ktime_set( 0, g_timeOutTimeMs*1000000 );
                    hrtimer_start( &g_timeOutTimer, ktime, HRTIMER_MODE_REL );
                }
                
                FL_Enable();
            } else {
                //CEI comments start
                //[BugID:1269]2015/03/13 Jiahan Li:To prevent the unnecessary function calls.
                if (g_led_state)
                    FL_Disable();
				if (g_timeOutTimeMs!=0)
                    hrtimer_cancel( &g_timeOutTimer );
                //CEI comments end
    	    }
            break;
        //CEI comments start
        //[BugID:370]: 2014/10/28 Jiahan Li: Don't do any low power detection
        case FLASH_IOC_HAS_LOW_POWER_DETECT:
    		PK_DBG("FLASH_IOC_HAS_LOW_POWER_DETECT");
    		temp=FL_hasLowPowerDetect();
    		if(copy_to_user((void __user *) arg , (void*)&temp , 4))
            {
                PK_DBG(" ioctl copy to user failed\n");
                return -1;
            }
            break;
        //CEI comments end
            
        default :
            PK_DBG(" No such command \n");
            i4RetValue = -EPERM;
            break;
    }
    return i4RetValue;
}




static int constant_flashlight_open(void *pArg)
{
    int i4RetValue = 0;
    PK_DBG("sky81290_flashlight_open line=%d\n", __LINE__);

	if (0 == strobe_Res)
	{
	    FL_Init();
		timerInit();
	}
	PK_DBG("sky81290_flashlight_open line=%d\n", __LINE__);
	spin_lock_irq(&g_strobeSMPLock);


    if(strobe_Res)
    {
        PK_ERR(" busy!\n");
        i4RetValue = -EBUSY;
    }
    else
    {
        strobe_Res += 1;
    }


    spin_unlock_irq(&g_strobeSMPLock);
    PK_DBG("sky81290_flashlight_open line=%d\n", __LINE__);

    return i4RetValue;

}


static int constant_flashlight_release(void *pArg)
{
    PK_DBG(" constant_flashlight_release\n");

    if (strobe_Res)
    {
        spin_lock_irq(&g_strobeSMPLock);

        strobe_Res = 0;
        strobe_Timeus = 0;

        /* LED On Status */
        g_strobe_On = FALSE;

        spin_unlock_irq(&g_strobeSMPLock);

    	FL_Uninit();
    }

    PK_DBG(" Done\n");

    return 0;

}


FLASHLIGHT_FUNCTION_STRUCT	constantFlashlightFunc=
{
	constant_flashlight_open,
	constant_flashlight_release,
	constant_flashlight_ioctl
};


MUINT32 constantFlashlightInit(PFLASHLIGHT_FUNCTION_STRUCT *pfFunc)
{
    if (pfFunc != NULL)
    {
        *pfFunc = &constantFlashlightFunc;
    }
    return 0;
}



/* LED flash control for high current capture mode*/
ssize_t strobe_VDIrq(void)
{

    return 0;
}

EXPORT_SYMBOL(strobe_VDIrq);

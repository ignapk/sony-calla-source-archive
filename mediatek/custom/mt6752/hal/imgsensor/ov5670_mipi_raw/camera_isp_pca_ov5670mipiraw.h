/*******************************************************************************
*   ISP_NVRAM_PCA_STRUCT
********************************************************************************/
Slider:{
    value:{// low middle high
               0,    0,   0,   0,   0,   0
    }
},
Config:{
    set:{//00
        0x00000000, 0x00000000
    }
}, 
PCA_LUTS:{
    lut_lo:{    // low
        //y_gain sat_gain hue_shift reserved
        {0x0, 0x0, 0x0, 0x0}, //  0
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 10
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 20
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 30
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 40
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 50
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 60
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 70
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 80
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 90
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //100
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //110
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //120
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //130
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //140
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //150
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //160
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //170
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0} //
    }, 
    lut_md:{    //  middle
        //y_gain sat_gain hue_shift reserved
        {0x0, 0xFD, 0x0, 0x0}, //  0
        {0x0, 0xEB, 0x0, 0x0}, //
        {0x0, 0xE6, 0x0, 0x0}, //
        {0x0, 0xE6, 0x0, 0x0}, //
        {0x0, 0xE5, 0x0, 0x0}, //
        {0x0, 0xE5, 0x0, 0x0}, //
        {0x0, 0xE4, 0x0, 0x0}, //
        {0x0, 0xE4, 0x0, 0x0}, //
        {0x0, 0xE3, 0x0, 0x0}, //
        {0x0, 0xE3, 0x0, 0x0}, //
        {0x0, 0xE2, 0x0, 0x0}, // 10
        {0x0, 0xE2, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE2, 0x0, 0x0}, // 20
        {0x0, 0xE2, 0x0, 0x0}, //
        {0x0, 0xE3, 0x0, 0x0}, //
        {0x0, 0xE3, 0x0, 0x0}, //
        {0x0, 0xE4, 0x0, 0x0}, //
        {0x0, 0xE6, 0x0, 0x0}, //
        {0x0, 0xE7, 0x0, 0x0}, //
        {0x0, 0xE8, 0x0, 0x0}, //
        {0x0, 0xE9, 0x0, 0x0}, //
        {0x0, 0xEB, 0x0, 0x0}, //
        {0x0, 0xED, 0x0, 0x0}, // 30
        {0x0, 0xEF, 0x0, 0x0}, //
        {0x0, 0xF1, 0x0, 0x0}, //
        {0x0, 0xF3, 0x0, 0x0}, //
        {0x0, 0xF5, 0x0, 0x0}, //
        {0x0, 0xF7, 0x0, 0x0}, //
        {0x0, 0xF8, 0x0, 0x0}, //
        {0x0, 0xFA, 0x0, 0x0}, //
        {0x0, 0xFC, 0x0, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, // 40
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 50
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 60
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 70
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 80
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 90
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //100
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //110
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //120
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //130
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //140
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //150
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //160
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //170
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0} //
    },
    lut_hi:{    //  high
        //y_gain sat_gain hue_shift reserved
        {0x0, 0xFD, 0x0, 0x0}, //  0
        {0x0, 0xEB, 0x0, 0x0}, //
        {0x0, 0xE6, 0x0, 0x0}, //
        {0x0, 0xE6, 0x0, 0x0}, //
        {0x0, 0xE5, 0x0, 0x0}, //
        {0x0, 0xE5, 0x0, 0x0}, //
        {0x0, 0xE4, 0x0, 0x0}, //
        {0x0, 0xE4, 0x0, 0x0}, //
        {0x0, 0xE3, 0x0, 0x0}, //
        {0x0, 0xE3, 0x0, 0x0}, //
        {0x0, 0xE2, 0x0, 0x0}, // 10
        {0x0, 0xE2, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE1, 0x0, 0x0}, //
        {0x0, 0xE2, 0x0, 0x0}, // 20
        {0x0, 0xE2, 0x0, 0x0}, //
        {0x0, 0xE3, 0x0, 0x0}, //
        {0x0, 0xE3, 0x0, 0x0}, //
        {0x0, 0xE4, 0x0, 0x0}, //
        {0x0, 0xE6, 0x0, 0x0}, //
        {0x0, 0xE7, 0x0, 0x0}, //
        {0x0, 0xE8, 0x0, 0x0}, //
        {0x0, 0xE9, 0x0, 0x0}, //
        {0x0, 0xEB, 0x0, 0x0}, //
        {0x0, 0xED, 0x0, 0x0}, // 30
        {0x0, 0xEF, 0x0, 0x0}, //
        {0x0, 0xF1, 0x0, 0x0}, //
        {0x0, 0xF3, 0x0, 0x0}, //
        {0x0, 0xF5, 0x0, 0x0}, //
        {0x0, 0xF7, 0x0, 0x0}, //
        {0x0, 0xF8, 0x0, 0x0}, //
        {0x0, 0xFA, 0x0, 0x0}, //
        {0x0, 0xFC, 0x0, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, // 40
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, // 50
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, // 60
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, // 70
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, // 80
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, // 90
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //100
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //110
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //120
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //130
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //140
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //150
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //160
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //170
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x00, 0x0}, //
        {0x0, 0x0, 0x01, 0x0}, //
        {0x0, 0x0, 0x01, 0x0}, //
        {0x0, 0x0, 0x01, 0x0}, //
        {0x0, 0x0, 0x01, 0x0}, //
        {0x0, 0x0, 0x01, 0x0}, //
        {0x0, 0x0, 0x01, 0x0}, //
        {0x0, 0x0, 0x00, 0x0} //
    },
    lut_lo2:{    // low2
        //y_gain sat_gain hue_shift reserved
        {0x0, 0x0, 0x0, 0x0}, //  0
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 10
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 20
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 30
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 40
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 50
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 60
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 70
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 80
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 90
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //100
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //110
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //120
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //130
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //140
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //150
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //160
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //170
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0} //
    }, 
    lut_md2:{    //  middle2
        //y_gain sat_gain hue_shift reserved
        {0x0, 0x0, 0x0, 0x0}, //  0
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 10
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 20
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 30
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 40
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 50
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 60
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 70
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 80
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 90
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //100
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //110
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //120
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //130
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //140
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //150
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //160
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //170
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0} //
    },
    lut_hi2:{    //  high2
        //y_gain sat_gain hue_shift reserved
                {0x0, 0x0, 0x0, 0x0}, //  0
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 10
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 20
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 30
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 40
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 50
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 60
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 70
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 80
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, // 90
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //100
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //110
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //120
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //130
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //140
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //150
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //160
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //170
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0}, //
        {0x0, 0x0, 0x0, 0x0} //
    }
        
}

